package com.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/addStocks")
public class addStocks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response){
	 	try{
		 	PrintWriter in = response.getWriter();
		 	in.println("Something went Wrong");
		 	in.close();
	 	}catch(Exception e){
	 		e.printStackTrace();
	 	}
	 }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");

	   response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
	   response.setHeader("pragma", "no-cache");
		HttpSession session = request.getSession();
		
		if(session.getAttribute("id")==null) {
			response.sendRedirect("index.html");
		}
		
		PrintWriter out = response.getWriter();
	
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		
		String name= request.getParameter("name");
		String author = request.getParameter("author");
		String price = request.getParameter("price");
        String count = request.getParameter("count");
		
		if(name.isEmpty() || author.isEmpty() || price.isEmpty() || count.isEmpty()) {
			out.println ("Invalid input filed cannot be empty");
			return;
		}
			
			int  p = Integer.parseInt(price);
			int c = Integer.parseInt(count);
		
		
		String query= "insert into Books"+"(BookName,AuthorName,Price,BookCount)"+"values(?,?,?,?)";
		
		
		try(Connection connection =DriverManager.getConnection(DbCredentials.dbUrl,DbCredentials.dbName,DbCredentials.dbPass)){
			PreparedStatement st = connection.prepareStatement(query);
			
			st.setString(1, name);
			st.setString(2, author);
			st.setInt(3, p);
			st.setInt(4, c);
			
			st.executeUpdate();
			
			session.removeAttribute("id");
			session.invalidate();
			out.println("Stocks Added into BooksStore");
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			out.close();
		}
	}

}
