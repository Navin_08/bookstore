package com.store;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter("/Login")
public class LoginFilter implements Filter{

	
   	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		

		PrintWriter out=response.getWriter();
		
		HttpServletRequest req = (HttpServletRequest) request;
	
		String name=  req.getParameter("name");
		String pass = req.getParameter("password");
		
			
			try{
				if(name.isEmpty() &&  pass.isEmpty()) {
					
					out.println("Field cannot be empty");
				}else {
					chain.doFilter(request, response);
				}
			}catch(Exception e) {
				out.println("Filtered Method not allowed");
			}finally {
				out.close();
			}

		
	}


}
