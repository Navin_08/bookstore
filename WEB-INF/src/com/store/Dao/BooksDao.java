//$Id$
package com.store.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.store.DbCredentials;
import com.store.model.Books;

public class BooksDao {
	
	public ArrayList<Books> getBooks() throws SQLException {
		
		ArrayList<Books>  books = new ArrayList<>();
		
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
       
		String query = "select * from Books";
		
       try(Connection connection = DriverManager.getConnection(DbCredentials.dbUrl,DbCredentials.dbName,DbCredentials.dbPass)){
           Statement statement  = connection.createStatement();
           ResultSet rs = statement.executeQuery(query);
           int i=0;
           
           while(rs.next()){
        	   int id=rs.getInt("bookId");
        	   String bn=rs.getString("BookName");
        	   String an=rs.getString("AuthorName");
        	   int price=rs.getInt("Price");
        	   int bookCount=rs.getInt("BookCount");
        	  
        	   books.add(i,new Books(id,bn,an,price,bookCount));
        	   i++;
           }
           
       }catch(Exception e) {
    	   System.out.println(e);
       }
		 
		return books;
	}

}
