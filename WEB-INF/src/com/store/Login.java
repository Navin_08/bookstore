package com.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.println("Method not allowed");
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	 response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
	   response.setHeader("pragma", "no-cache");
	 			PrintWriter out=response.getWriter();
	 			
	 			String name =request.getParameter("name");
				String password = request.getParameter("password");
				String query = "select password from customers where username=?";
					
				 try {
						Class.forName("com.mysql.cj.jdbc.Driver");
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					}
				 
				try(Connection con  = DriverManager.getConnection(DbCredentials.dbUrl,DbCredentials.dbName,DbCredentials.dbPass)) {
					PreparedStatement preparedStatement = con.prepareStatement(query);
				preparedStatement.setString(1,name);
					ResultSet rs = preparedStatement.executeQuery();
					
					if(rs.next()){
							String pass = rs.getString(1);
							if(pass.equals(password)) {
								HttpSession session = request.getSession();
								session.setAttribute("session-id", name);
								//request.setAttribute("name", name);
								//request.getRequestDispatcher("/Store").include(request,response);
								response.sendRedirect("/bookstore/Store?name="+name);
								
							}else {
								out.println("Username or password is Wrong");
							}
					
					}else{
						out.println("Username or password is wrong");
					}
	
				} catch (Exception e) {
					out.println(e);
				}

		 out.close();
	}

}
