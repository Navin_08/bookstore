package com.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/Admin")
public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    protected void doGet(HttpServletRequest request, HttpServletResponse response){
	 	try{
		 	PrintWriter in = response.getWriter();
		 	in.println("Something went Wrong");
		 	in.close();
	 	}catch(Exception e){
	 		e.printStackTrace();
	 	}
	 }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			 response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
	         response.setHeader("pragma", "no-cache");
			
			String adminName =  request.getParameter("name");
			String adminPass = request.getParameter("password");
			
 			PrintWriter out=response.getWriter();


 			
			if(adminName.isEmpty() && adminPass.isEmpty()) {
				out.println("filed cannot be empty");
			}
				
			String query = "select pass from admin where name=?";
			
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}
			
			try(Connection con  = DriverManager.getConnection(DbCredentials.dbUrl,DbCredentials.dbName,DbCredentials.dbPass)) {
				PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1,adminName);
				ResultSet rs = preparedStatement.executeQuery();
				
				if(rs.next()){
						String pass = rs.getString(1);
						if(pass.equals(adminPass)) {
							out.println("Login success");
							
							HttpSession session = request.getSession();
							session.setAttribute("id",adminName);
							response.sendRedirect("/bookstore/addStocks.html");
							
						}else {
							out.println("Username or password is Wrong");
						}
				
				}else{
					out.println("Not found");
				}

			} catch (Exception e) {
				out.println(e);
			}finally {
				out.close();
			}
				
			}
			
}
