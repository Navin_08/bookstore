package com.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Dashboard")
public class Dashboard extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 HttpSession session = request.getSession();
		 	
		 if(session.getAttribute("session-id")==null ){
			 response.sendRedirect("Login.html");
		 }
		
       response.setContentType("text/html");
       response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
	   response.setHeader("pragma", "no-cache");
       PrintWriter out  = response.getWriter();
       
       
       String query = "select * from Books";
       out.print ("<table>");
       out.print ("<caption>Book Details:</caption>");
       out.print("<table><tr><th>BookId</th><th>BookName</th><th>AuthorName</th><th>price</th><th>BookCount</th></tr>");
       
       try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
       
       try(Connection connection = DriverManager.getConnection(DbCredentials.dbUrl,DbCredentials.dbName,DbCredentials.dbPass)){
           Statement statement  = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()){
                   // out.print("test");
                    out.print("<tr><td>");
                    out.print(rs.getInt(1));
            
                    out.print("</td>");
                    out.print("<td>");
                    out.print(rs.getString(2));
                    
                    out.print("</td>");
                    out.print("<td>");
                    out.print(rs.getString(3));
                    out.print("</td>");
                    out.print("<td>");
                    out.print(rs.getInt(4));
                    out.print("</td>");
                    out.print("<td>");
                    out.println(rs.getInt(5));
                    out.print("</td></tr>");
            }
            out.print("</table>");
            
            out.print("<form action=\"/bookstore/BuyBooks\" method=\"post\">\n" + 
     				"			<label for=\"name\">Enter the bookname</label><br>\n" + 
     				"        	<input type=\"text\" name=\"name\" id=\"name\"><br><br>\n" + 
     				"<label for=\"count\">Enter the count you want</label><br>\n" + 
     						" <input type=\"text\" name=\"count\"  id=\"count\"><br><br>\n"+
     				"<input type=\"submit\" value=\"submit\">\n" + 
     						"</form>"+
     				"<a href=\"/bookstore/Logout\" > <input type=\"submit\" value=\"Logout\"></a>");
            
        }catch (Exception e){
            System.out.println(e);
        }finally {
        	out.close();
        }
	}
		
}


