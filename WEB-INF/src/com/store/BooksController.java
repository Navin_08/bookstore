package com.store;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.store.Dao.BooksDao;
import com.store.model.Books;


@WebServlet("/BooksController")
public class BooksController extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
	   response.setHeader("pragma", "no-cache");
		HttpSession session = request.getSession();
		
		if(session.getAttribute("session-id")==null) {
			response.sendRedirect("Login.html");
		}
		
		BooksDao dao = new BooksDao();
		ArrayList<Books> book = new ArrayList<>();
		
		
		try {
			book = dao.getBooks();
			request.setAttribute("books",book);
//			response.sendRedirect("/bookstore/Books?bo="+book);
			// RequestDispatcher rd = request.getRequestDispatcher("/ShowBooks");
			// rd.forward(request, response);
		    request.getRequestDispatcher("/ShowBooks").include(request,response);
		} catch (SQLException e) {
			e.printStackTrace();
		}


		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doGet(request, response);
	}

}
