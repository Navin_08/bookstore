package com.store;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.text.StringEscapeUtils;


@WebServlet("/Store")
public class Store extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	static String name;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		 
           response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
	   response.setHeader("pragma", "no-cache");
		
		name = request.getParameter("name");
		name = StringEscapeUtils.escapeHtml4(name);
		 
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
		response.setHeader("pragma", "no-cache");

		HttpSession session = request.getSession();
		 	
		 if(session.getAttribute("session-id")==null){
			 response.sendRedirect("Login.html");
		 }
		 
         PrintWriter out = response.getWriter();
         out.println("<!DOC TYPE html>\n" +
                 "<html>\n" +
                 "<head>\n" +
                 "    <meta charset=\"UTF-8\">\n" +
                 "    <title>BookStore</title>\n" +
                 "</head>\n" +
                 "<style>\n" +
                 "\tbody{\n" +
                 "\t\ttext-align: center;\n" +
                 "\t}\n" +
                 "\t</style>\n" +
                 "<body>\n" +
                 "          <h1>Welcome,"+ name +"</h1>\n" +
                 "          <a href=\"/bookstore/BooksController\" > <input type=\"submit\" value=\"Store\"></a>\n" +
                 "<a href=\"/bookstore/Logout\" > <input type=\"submit\" value=\"Logout\"></a>"+
                 "</body>\n" +
                 "</html>");

     out.close();
	}

}
