package com.store.model;

public class Books {
		private int bookId;
		private int bookCount;
		private String bookName;
		private String authorName;
		private int price;
		
		public Books(int bookId,String bookName,String authorName,int price,int bookCount) {
			this.authorName=authorName;
			this.bookId=bookId;
			this.bookCount=bookCount;
			this.price=price;
			this.bookName=bookName;
		}
		
		public Books(){}
		
		public int getBookId() {
			return bookId;
		}
		public void setBookId(int bookId) {
			this.bookId = bookId;
		}
		public int getBookCount() {
			return bookCount;
		}
		public void setBookCount(int bookCount) {
			this.bookCount = bookCount;
		}
		public String getBookName() {
			return bookName;
		}
		public void setBookName(String bookName) {
			this.bookName = bookName;
		}
		public String getAuthorName() {
			return authorName;
		}
		public void setAuthorName(String authorName) {
			this.authorName = authorName;
		}
		public int getPrice() {
			return price;
		}
		public void setPrice(int price) {
			this.price = price;
		}
		
		
		@Override
		public String toString() {
			return "bookId=" + bookId + ",bookCount=" + bookCount + ",bookName=" + bookName + ",authorName=" + authorName + ",price=" + price;
		}
		
}
