package com.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.*;  

@WebServlet("/SignIn")
public class SignIn extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public static final String regex ="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,20}";

     protected void doGet(HttpServletRequest request, HttpServletResponse response){
        try{
            PrintWriter in = response.getWriter();
            in.println("Something went Wrong");
            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
     }
    

    public static boolean isvalid(String pass){
        Pattern p = Pattern.compile(regex);
        Matcher match = p.matcher(pass);
        return match.matches();
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
        response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
       response.setHeader("pragma", "no-cache");
                PrintWriter out = response.getWriter();

                response.setContentType("text/html");

                String uname = request.getParameter("uname");
                String password = request.getParameter("password");

                boolean  check = isvalid(password);  

                if(check){
                    String query = "insert into customers"+"(username,password)"+ "values(?,?)";

                    try {
                    	Class.forName("com.mysql.cj.jdbc.Driver");
                    } catch (ClassNotFoundException e1) {
                    	e1.printStackTrace();
                    }
                        
                    try (Connection connection = DriverManager
                            .getConnection(DbCredentials.dbUrl,DbCredentials.dbName,DbCredentials.dbPass);
                         PreparedStatement preparedStatement = connection.prepareStatement(query)) {

                        preparedStatement.setString(1,uname);
                        preparedStatement.setString(2,password);

                        if(uname.isEmpty() && password.isEmpty()){
                            out.println("Username or Password cannot be empty");
                            return;
                        }else{
                            preparedStatement.executeUpdate();
                            response.sendRedirect("index.html");
                        }

                    } catch (Exception e) {
                        out.println(e);
                    }
                out.close();
            }else{
                out.println("Password must contains minumum 8 Character,1 Lowercase,1 Uppercase,1 special Character");
                out.close();
            }
    }
}
