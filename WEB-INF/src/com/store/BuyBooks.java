package com.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/BuyBooks")
public class BuyBooks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	 static int bookCount=0;

	 protected void doGet(HttpServletRequest request, HttpServletResponse response){
	 	try{
		 	PrintWriter in = response.getWriter();
		 	in.println("Something went Wrong");
		 	in.close();
	 	}catch(Exception e){
	 		e.printStackTrace();
	 	}
	 }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
	   response.setHeader("pragma", "no-cache");
		int cost=0;
		HttpSession session = request.getSession();
	 		
		String cus = (String) session.getAttribute("session-id");
		 if(session.getAttribute("session-id")==null){
			 response.sendRedirect("Login.html");
		 }
		
		response.setContentType("text/html");
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
		response.setHeader("pragma", "no-cache");
	
	
		PrintWriter out = response.getWriter();
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		
		String name= request.getParameter("name");
		
        String count = request.getParameter("count");
		
		if(name.isEmpty() && count.isEmpty()) {
			out.print("Invalid input filed cannot be empty");
			return;
		}
		
		String query= "select BookCount from Books where BookName=?";
		String price="select Price from Books Where Bookname=?";
		boolean flag=false;
		
		try(Connection connection =DriverManager.getConnection(DbCredentials.dbUrl,DbCredentials.dbName,DbCredentials.dbPass)){
			PreparedStatement st = connection.prepareStatement(price);
			ResultSet p = st.executeQuery();
			
			st.setString(1, name);
	    
			if(p.next()) {
			  cost=p.getInt(1);
			  //out.println(cost);
			}else {
				out.println("Empty set");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		try(Connection connection  = DriverManager.getConnection(DbCredentials.dbUrl,DbCredentials.dbName,DbCredentials.dbPass)){
			PreparedStatement statement = connection.prepareStatement(query);

			statement.setString(1, name);
			ResultSet rs = statement.executeQuery();
				
				if(rs.next()) {
					bookCount=rs.getInt(1);

				}else {
					flag=true;
					out.print("book not found");
			}
					
			PreparedStatement preparestatement = connection.prepareStatement("update Books set BookCount=? where BookName=?");
			int c = Integer.parseInt(count);
			out.println(c);
			int cu=0;
			
			if(c>bookCount && !flag) {
				out.println("These many stocks are not available");
				return ;
			}
			else {
				cu =bookCount-Integer.parseInt(count);
				//out.println(cu);
			}
			
			preparestatement.setInt(1,cu);
			preparestatement.setString(2,name);
		
			preparestatement.executeUpdate();
			
			if(!flag) {
				out.println(cus);
				out.println("Total Price: "+ (cu*cost));
				out.println("<h1>book purchased</h1>");
			}
		}catch(Exception e) {
			System.out.println(e);
		}finally {
			out.close();
		}
	}
}
