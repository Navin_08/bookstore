package com.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.store.model.Books;


@WebServlet("/ShowBooks")
public class ShowBooks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
	   response.setHeader("pragma", "no-cache");
		HttpSession session = request.getSession();
		
		if(session.getAttribute("session-id")==null) {
			response.sendRedirect("Login.html");
		}
		PrintWriter out = response.getWriter();

		try {
			ArrayList<Books>  bo = (ArrayList<Books>) request.getAttribute("books");
			// Gson gson = new Gson();
			// GsonBuilder gsonBuilder = new GsonBuilder();
			// String obj = gson.toJson(bo);
			// out.println(obj);
			out.print ("<table>");
		       out.print ("<caption>Book Details:</caption>");
		       out.print("<table><tr><th>BookId</th><th>BookName</th><th>AuthorName</th><th>price</th><th>BookCount</th></tr>");
			for(int i=0;i<bo.size();i++) {
				out.print("<tr><td>");
                out.print(bo.get(i).getBookId());
        
                out.print("</td>");
                out.print("<td>");
                out.print(bo.get(i).getBookName());
                
                out.print("</td>");
                out.print("<td>");
                out.print(bo.get(i).getAuthorName());
                out.print("</td>");
                out.print("<td>");
                out.print(bo.get(i).getPrice());
                out.print("</td>");
                out.print("<td>");
                out.println(bo.get(i).getBookCount());
                out.print("</td></tr>");
			}
			 out.print("</table>");
			 out.print("<form action=\"/bookstore/BuyBooks\" method=\"post\">\n" + 
	     				"			<label for=\"name\">Enter the bookname</label><br>\n" + 
	     				"        	<input type=\"text\" name=\"name\" id=\"name\"><br><br>\n" + 
	     				"<label for=\"count\">Enter the count you want</label><br>\n" + 
	     						" <input type=\"text\" name=\"count\"  id=\"count\"><br><br>\n"+
	     				"<input type=\"submit\" value=\"submit\">\n" + 
	     						"</form>"+
	     				"<a href=\"/bookstore/Logout\" > <input type=\"submit\" value=\"Logout\"></a>");
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
